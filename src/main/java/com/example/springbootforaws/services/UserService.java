package com.example.springbootforaws.services;

import com.example.springbootforaws.models.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static List<User> users = new ArrayList<>();
    static {
        users.add(new User("Avrora", "Sidney", 20));
        users.add(new User("Mihael", "Shumaher", 34));
        users.add(new User("Jems", "Bond", 99));
    }

    public User getUser(Long userId){
        for(User user : users){
            if(user.getId().equals(userId)){
                return user;
            }
        }
        throw new IllegalArgumentException("User with such id was not found");
    }

    public List<User> getAllUsers(){
        return users;
    }

    public void deleteUser(Long userId){
        users.removeIf(user -> user.getId().equals(userId));
    }

    public void createUser(User user){
        users.add(user);
    }
}
